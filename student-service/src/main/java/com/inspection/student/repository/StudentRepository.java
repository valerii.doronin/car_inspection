package com.inspection.student.repository;

import com.inspection.student.model.Student;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Optional;

public interface StudentRepository extends JpaRepository<Student, Long> {

    Student findByEmail(String email);

    @Query("SELECT DISTINCT s from Student s "
            + "WHERE s.id = :id")
    Optional<Student> findById(@Param("id") Long id);

}

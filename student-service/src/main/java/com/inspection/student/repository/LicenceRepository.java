package com.inspection.student.repository;

import com.inspection.student.model.Licence;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LicenceRepository extends JpaRepository<Licence, Long> {

    Licence findByNumber(String number);

}

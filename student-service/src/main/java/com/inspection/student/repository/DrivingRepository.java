package com.inspection.student.repository;

import com.inspection.student.model.Driving;
import com.inspection.student.model.Student;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DrivingRepository extends JpaRepository<Driving, Long> {

    void deleteByStudent(Student student);

}

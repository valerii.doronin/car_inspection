package com.inspection.student.repository;

import com.inspection.student.model.Question;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Set;

public interface QuestionRepository extends JpaRepository<Question, Long> {

    @Query("SELECT q from Question q "
            + "LEFT JOIN FETCH q.options "
            + "ORDER BY rand()")
    List<Question> findRandomQuestions(Pageable pageable);

    @Query("SELECT DISTINCT q from Question q "
            + "LEFT JOIN FETCH q.options "
            + "WHERE q.id IN :ids ")
    List<Question> findQuestionByIds(@Param("ids") Set<Long> ids);

}

package com.inspection.student.repository;

import com.inspection.student.model.Group;
import com.inspection.student.model.Student;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.time.LocalDateTime;
import java.util.Optional;

public interface GroupRepository extends JpaRepository<Group, Long> {

    @Modifying
    @Query(nativeQuery = true,
    value = "DELETE from group_students "
            + "WHERE student_id = :studentId")
    void deleteByStudentId(@Param("studentId") Long studentId);

    @Query("SELECT DISTINCT g from Group g "
            + "LEFT JOIN FETCH g.students "
            + "WHERE g.startTime > :minTime "
            + "AND g.startTime < :maxTime "
            + "AND :student MEMBER OF g.students")
    Optional<Group> findCurrentByStudent(@Param("minTime") LocalDateTime minTime,
                                         @Param("maxTime") LocalDateTime maxTime,
                                         @Param("student") Student student);

}

package com.inspection.student.repository;

import com.inspection.student.model.LicenceCategory;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LicenceCategoryRepository extends JpaRepository<LicenceCategory, Long> {

    LicenceCategory findByName(String name);

}

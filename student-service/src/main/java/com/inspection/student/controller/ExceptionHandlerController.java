package com.inspection.student.controller;

import com.inspection.student.dto.ErrorEntity;
import com.inspection.student.exceptions.EntityNotFoundException;
import com.inspection.student.exceptions.ForbiddenException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class ExceptionHandlerController {

    @ExceptionHandler(EntityNotFoundException.class)
    public ResponseEntity<ErrorEntity> entityNotFoundExceptionHandler(EntityNotFoundException ex) {
        return new ResponseEntity<>(new ErrorEntity(ex.getMessage(), HttpStatus.BAD_REQUEST.name()), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(ForbiddenException.class)
    public ResponseEntity<ErrorEntity> forbiddenExceptionHandler(ForbiddenException ex) {
        return new ResponseEntity<>(new ErrorEntity(ex.getMessage(), HttpStatus.FORBIDDEN.name()), HttpStatus.FORBIDDEN);
    }

}

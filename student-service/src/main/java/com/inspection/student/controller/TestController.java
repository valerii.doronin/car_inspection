package com.inspection.student.controller;

import com.inspection.student.dto.AnswerDto;
import com.inspection.student.dto.PrincipalDto;
import com.inspection.student.dto.QuestionDto;
import com.inspection.student.service.QuestionService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@AllArgsConstructor
public class TestController {

    private final QuestionService questionService;

    @GetMapping("questions")
    public ResponseEntity<List<QuestionDto>> getQuestions(@RequestBody PrincipalDto dto) {
        return new ResponseEntity<>(questionService.findRandomQuestions(dto.getPrincipal()), HttpStatus.OK);
    }

    @PostMapping("questions")
    public ResponseEntity<Boolean> passTest(@RequestBody AnswerDto answer) {
        return new ResponseEntity<>(questionService.passTest(answer), HttpStatus.OK);
    }

}

package com.inspection.student.controller;


import com.inspection.student.dto.DrivingResultDto;
import com.inspection.student.service.DrivingService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@AllArgsConstructor
public class DrivingController {

    private final DrivingService drivingService;

    @PostMapping(value = "drivings/results")
    public ResponseEntity<?> passDriving(@RequestBody DrivingResultDto dto) {
        drivingService.passDriving(dto);
        return new ResponseEntity<>(HttpStatus.OK);
    }

}

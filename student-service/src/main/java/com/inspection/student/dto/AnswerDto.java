package com.inspection.student.dto;

import lombok.Data;

import java.util.List;

@Data
public class AnswerDto extends PrincipalDto {

    private List<QuestionAnswerDto> questions;

}

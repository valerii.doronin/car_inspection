package com.inspection.student.dto;

import lombok.Data;

@Data
public class DrivingResultDto {

    private Long studentId;
    private Boolean passed;
    
}

package com.inspection.student.dto;

import lombok.Data;

@Data
public class QuestionAnswerDto {

    private Long questionId;
    private Long answerId;

}

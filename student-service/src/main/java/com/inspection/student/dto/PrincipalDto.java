package com.inspection.student.dto;

import lombok.Data;

@Data
public class PrincipalDto {

    private String principal;

}

package com.inspection.student.dto;

import lombok.Data;

@Data
public class OptionDto {

    private Long id;
    private String value;

}

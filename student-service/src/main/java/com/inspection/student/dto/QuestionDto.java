package com.inspection.student.dto;

import lombok.Data;

import java.util.List;

@Data
public class QuestionDto {

    private Long id;
    private String value;
    private List<OptionDto> options;

}

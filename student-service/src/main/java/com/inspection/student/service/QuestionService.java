package com.inspection.student.service;

import com.inspection.student.dto.AnswerDto;
import com.inspection.student.dto.QuestionDto;

import java.util.List;

public interface QuestionService {

    List<QuestionDto> findRandomQuestions(String email);

    boolean passTest(AnswerDto answer);

}

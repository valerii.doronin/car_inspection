package com.inspection.student.service;

import com.inspection.student.dto.DrivingResultDto;

public interface DrivingService {

    void passDriving(DrivingResultDto dto);

}

package com.inspection.student.service.impl;

import com.inspection.student.dto.DrivingResultDto;
import com.inspection.student.exceptions.EntityNotFoundException;
import com.inspection.student.model.Driver;
import com.inspection.student.model.Licence;
import com.inspection.student.model.Student;
import com.inspection.student.repository.DriverRepository;
import com.inspection.student.repository.DrivingRepository;
import com.inspection.student.repository.LicenceCategoryRepository;
import com.inspection.student.repository.LicenceRepository;
import com.inspection.student.repository.StudentRepository;
import com.inspection.student.repository.UserRepository;
import com.inspection.student.service.DrivingService;
import com.mifmif.common.regex.Generex;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.Collections;

@Service
@AllArgsConstructor
public class DrivingServiceImpl implements DrivingService {

    private final StudentRepository studentRepository;
    private final UserRepository userRepository;
    private final LicenceCategoryRepository licenceCategoryRepository;
    private final LicenceRepository licenceRepository;
    private final DriverRepository driverRepository;
    private final DrivingRepository drivingRepository;

    @Override
    @Transactional
    public void passDriving(DrivingResultDto dto) {
        if (dto.getPassed()) {
            Student student = studentRepository.findById(dto.getStudentId()).orElseThrow(() -> new EntityNotFoundException("Студента не знайдено"));
            Licence licence = new Licence();
            licence.setNumber(generateTransportNumber());
            licence.setStartDate(LocalDate.now());
            licence.setFinishDate(LocalDate.now().plusYears(2));
            Driver driver = new Driver();
            driver.setBirthday(student.getBirthday());
            driver.setName(student.getName());
            driver.setSurname(student.getSurname());
            driver.setPassport(student.getPassport());
            driver.setCategories(Collections.singleton(licenceCategoryRepository.findByName("B1")));
            driver.setLicence(licenceRepository.save(licence));
            drivingRepository.deleteByStudent(student);
            driverRepository.save(driver);
            userRepository.delete(student);
        }
    }

    private String generateTransportNumber() {
        Generex generex = new Generex("[А-ЕЖ-ЩЮЯІ]{3} \\d{6}");
        Licence licence;
        String number;
        do {
            number = generex.random();
            licence = licenceRepository.findByNumber(number);
        } while (licence != null);
        return number;
    }
}

package com.inspection.student.service.impl;

import com.inspection.student.dto.AnswerDto;
import com.inspection.student.dto.QuestionAnswerDto;
import com.inspection.student.dto.QuestionDto;
import com.inspection.student.exceptions.ForbiddenException;
import com.inspection.student.model.Group;
import com.inspection.student.model.Option;
import com.inspection.student.model.Question;
import com.inspection.student.model.Student;
import com.inspection.student.repository.DrivingRepository;
import com.inspection.student.repository.GroupRepository;
import com.inspection.student.repository.QuestionRepository;
import com.inspection.student.repository.StudentRepository;
import com.inspection.student.service.QuestionService;
import lombok.AllArgsConstructor;
import org.springframework.core.convert.ConversionService;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class QuestionServiceImpl implements QuestionService {

    private final QuestionRepository questionRepository;
    private final DrivingRepository drivingRepository;
    private final StudentRepository studentRepository;
    private final GroupRepository groupRepository;
    private final ConversionService conversionService;

    @Override
    public List<QuestionDto> findRandomQuestions(String email) {
        Optional<Group> group = groupRepository.findCurrentByStudent(
                LocalDateTime.now().plusMinutes(-15),
                LocalDateTime.now().plusMinutes(15),
                studentRepository.findByEmail(email));
        group.orElseThrow(() -> new ForbiddenException("Тест для вас не доступний"));
        List<Question> questions = questionRepository.findRandomQuestions(PageRequest.of(0, 10));
        return questions.stream()
                .map(question -> conversionService.convert(question, QuestionDto.class)).collect(Collectors.toList());
    }

    @Override
    @Transactional
    public boolean passTest(AnswerDto answer) {
        Map<Long, Long> questionsMap = questionRepository.findQuestionByIds(answer.getQuestions()
                .stream().map(QuestionAnswerDto::getQuestionId).collect(Collectors.toSet()))
                .stream().collect(Collectors.toMap(Question::getId, this::getCorrectOptionId));

        boolean result = true;
        int mistakes = 0;
        for (QuestionAnswerDto answerQuestion : answer.getQuestions()) {
            if (!questionsMap.get(answerQuestion.getQuestionId()).equals(answerQuestion.getAnswerId()) && ++mistakes > 1) {
                result = false;
                break;
            }
        }
        if (result) {
            testSuccessful(answer.getPrincipal());
        }
        return result;
    }

    private long getCorrectOptionId(Question q) {
        return q.getOptions().stream().filter(Option::isCorrect).map(Option::getId).findFirst().get();
    }

    private void testSuccessful(String email) {
        Student student = studentRepository.findByEmail(email);
        groupRepository.deleteByStudentId(student.getId());
        student.setPassedTest(true);
    }

}

package com.inspection.student.converters;

import com.inspection.student.dto.OptionDto;
import com.inspection.student.model.Option;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class OptionToDtoConverter implements Converter<Option, OptionDto> {

    @Override
    public OptionDto convert(Option option) {
        OptionDto dto = new OptionDto();
        dto.setId(option.getId());
        dto.setValue(option.getValue());
        return dto;
    }
}

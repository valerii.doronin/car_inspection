package com.inspection.student.converters;

import com.inspection.student.dto.OptionDto;
import com.inspection.student.dto.QuestionDto;
import com.inspection.student.model.Question;
import lombok.AllArgsConstructor;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.util.Comparator;
import java.util.stream.Collectors;

@Component
@AllArgsConstructor
public class QuestionToDtoConverter implements Converter<Question, QuestionDto> {

    private final OptionToDtoConverter converter;

    @Override
    public QuestionDto convert(Question question) {
        QuestionDto dto = new QuestionDto();
        dto.setId(question.getId());
        dto.setValue(question.getValue());
        dto.setOptions(question.getOptions().stream()
                .map(converter::convert)
                .sorted(Comparator.comparing(OptionDto::getId))
                .collect(Collectors.toList()));
        return dto;
    }
}

package com.inspection.driver.controller;

import com.inspection.driver.dto.CarAddDto;
import com.inspection.driver.dto.CarDto;
import com.inspection.driver.dto.DriverGeneralDto;
import com.inspection.driver.dto.DriverViolationDto;
import com.inspection.driver.dto.DrivingCreateDto;
import com.inspection.driver.service.DriverService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@AllArgsConstructor
public class DriverController {

    private final DriverService driverService;

    @GetMapping("drivers")
    public ResponseEntity<List<DriverGeneralDto>> findGeneralInfo() {
        return new ResponseEntity<>(driverService.findGeneralInfo(), HttpStatus.OK);
    }

    @GetMapping(value = "drivers/{id}")
    public ResponseEntity<DriverViolationDto> findById(@PathVariable("id") Long id) {
        return new ResponseEntity<>(driverService.findById(id), HttpStatus.OK);
    }

    @PostMapping(value = "drivers/cars")
    public ResponseEntity<String> addCar(@RequestBody CarAddDto carDto) {
        return new ResponseEntity<>(driverService.addCar(carDto), HttpStatus.OK);
    }


}


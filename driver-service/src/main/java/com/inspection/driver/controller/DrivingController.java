package com.inspection.driver.controller;

import com.inspection.driver.dto.DrivingCreateDto;
import com.inspection.driver.dto.DrivingDto;
import com.inspection.driver.dto.DrivingFormDto;
import com.inspection.driver.service.DrivingService;
import com.inspection.driver.service.UserService;
import lombok.AllArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.List;

@RestController
@AllArgsConstructor
public class DrivingController {

    private final DrivingService drivingService;
    private final UserService userService;

    @GetMapping(value = "drivings")
    public ResponseEntity<List<DrivingDto>> findByDateTime(@RequestParam("date") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Date date) {
        return new ResponseEntity<>(drivingService.findByDate(date), HttpStatus.OK);
    }

    @GetMapping(value = "drivings/info")
    public ResponseEntity<DrivingFormDto> getDrivingFormData() {
        return new ResponseEntity<>(userService.getDrivingForm(), HttpStatus.OK);
    }

    @PostMapping(value = "drivings")
    public ResponseEntity<?> createDriving(@RequestBody DrivingCreateDto drivingCreateDto) {
        drivingService.createDriving(drivingCreateDto);
        return new ResponseEntity(HttpStatus.OK);
    }

}

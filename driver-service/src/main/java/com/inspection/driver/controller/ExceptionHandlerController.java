package com.inspection.driver.controller;

import com.inspection.driver.dto.ErrorEntity;
import com.inspection.driver.exceptions.EntityNotFoundException;
import com.inspection.driver.exceptions.InvalidParamException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class ExceptionHandlerController {

    @ExceptionHandler(EntityNotFoundException.class)
    public ResponseEntity<ErrorEntity> entityNotFoundExceptionHandler(EntityNotFoundException ex) {
        return new ResponseEntity<>(new ErrorEntity(ex.getMessage(), HttpStatus.BAD_REQUEST.name()), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(InvalidParamException.class)
    public ResponseEntity<ErrorEntity> invalidParamExceptionHandler(InvalidParamException ex) {
        return new ResponseEntity<>(new ErrorEntity(ex.getMessage(), HttpStatus.BAD_REQUEST.name()), HttpStatus.BAD_REQUEST);
    }

}

package com.inspection.driver.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Set;

@Data
@Entity
@Table(name = "drivers")
@EqualsAndHashCode(of = "passport")
@ToString(exclude = {"violations", "cars", "licence", "categories"})
public class Driver {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "surname")
    private String surname;

    @Column(name = "name")
    private String name;

    @Column(name = "passport")
    private String passport;

    @Column(name = "deprived")
    private LocalDateTime deprived;

    @Column(name = "birthday")
    private LocalDate birthday;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "driver")
    private List<Violation> violations;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "driver")
    private Set<Car> cars;

    @OneToOne
    @JoinColumn(name = "licence_id")
    private Licence licence;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "driver_categories",
            joinColumns = {@JoinColumn(name = "driver_id", referencedColumnName = "id")},
            inverseJoinColumns = {@JoinColumn(name = "category_id", referencedColumnName = "id")})
    private Set<LicenceCategory> categories;

}

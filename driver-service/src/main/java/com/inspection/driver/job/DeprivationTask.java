package com.inspection.driver.job;

import com.inspection.driver.model.Driver;
import com.inspection.driver.repository.DriverRepository;
import lombok.AllArgsConstructor;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;

@Component
@AllArgsConstructor
public class DeprivationTask {

    private final DriverRepository driverRepository;

    @Scheduled(fixedRate = 5000)
    @Transactional
    public void deprive() {
        List<Driver> drivers = driverRepository.findAllForDepriving(LocalDateTime.now());
        for (Driver driver : drivers) {
            driver.setDeprived(LocalDateTime.now().plusMinutes(1));
        }
    }

    @Scheduled(fixedRate = 5000)
    @Transactional
    public void restore() {
        List<Driver> drivers = driverRepository.findAllForRestoring(LocalDateTime.now());
        for (Driver driver : drivers) {
            driver.setDeprived(null);
        }
    }

}

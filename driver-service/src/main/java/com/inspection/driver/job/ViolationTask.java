package com.inspection.driver.job;

import com.inspection.driver.model.Article;
import com.inspection.driver.model.Driver;
import com.inspection.driver.model.Violation;
import com.inspection.driver.repository.ArticleRepository;
import com.inspection.driver.repository.DriverRepository;
import com.inspection.driver.repository.ViolationRepository;
import com.inspection.driver.util.RandomEntity;
import lombok.AllArgsConstructor;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;

@Component
@AllArgsConstructor
public class ViolationTask {

    private final RandomEntity randomEntity;
    private final ViolationRepository violationRepository;

    @Scheduled(fixedRate = 5000)
    @Transactional
    public void fine() {
        Article article = randomEntity.getRandomEntity(ArticleRepository.class);
        Driver driver = randomEntity.getRandomEntity(DriverRepository.class);
        Violation violation = new Violation();
        violation.setDriver(driver);
        violation.setArticle(article);
        violation.setTime(LocalDateTime.now());
        violationRepository.save(violation);
    }

    @Scheduled(fixedRate = 5000)
    @Transactional
    public void pay() {
        Violation violation = this.randomEntity.getRandomEntity(ViolationRepository.class);
        violationRepository.delete(violation);
    }

}

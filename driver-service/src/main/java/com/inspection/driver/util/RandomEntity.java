package com.inspection.driver.util;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Lookup;
import org.springframework.context.ApplicationContext;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

@Component
public class RandomEntity {

    @Autowired
    private ApplicationContext context;

    public <E> E getRandomEntity(Class<? extends JpaRepository<E, Long>> clazz) {
        JpaRepository<E, Long> repository = context.getBean(clazz);
        int index = (int) (Math.random() * repository.count());
        return repository.findAll(PageRequest.of(index, 1)).getContent().get(0);
    }

}

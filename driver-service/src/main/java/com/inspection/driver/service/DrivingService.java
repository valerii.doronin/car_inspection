package com.inspection.driver.service;

import com.inspection.driver.dto.DrivingCreateDto;
import com.inspection.driver.dto.DrivingDto;

import java.util.Date;
import java.util.List;

public interface DrivingService {

    List<DrivingDto> findByDate(Date drivingDate);

    void createDriving(DrivingCreateDto dto);

}

package com.inspection.driver.service;

import com.inspection.driver.dto.CarAddDto;
import com.inspection.driver.dto.DriverGeneralDto;
import com.inspection.driver.dto.DriverViolationDto;

import java.util.List;

public interface DriverService {

    List<DriverGeneralDto> findGeneralInfo();

    DriverViolationDto findById(Long id);

    String addCar(CarAddDto carDto);

}

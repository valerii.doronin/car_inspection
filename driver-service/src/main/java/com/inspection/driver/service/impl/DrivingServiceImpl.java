package com.inspection.driver.service.impl;

import com.inspection.driver.dto.DrivingCreateDto;
import com.inspection.driver.dto.DrivingDto;
import com.inspection.driver.exceptions.EntityNotFoundException;
import com.inspection.driver.exceptions.InvalidParamException;
import com.inspection.driver.model.Admin;
import com.inspection.driver.model.Driving;
import com.inspection.driver.repository.AdminRepository;
import com.inspection.driver.repository.DrivingRepository;
import com.inspection.driver.repository.StudentRepository;
import com.inspection.driver.service.DrivingService;
import lombok.AllArgsConstructor;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class DrivingServiceImpl implements DrivingService {

    private final DrivingRepository drivingRepository;
    private final AdminRepository adminRepository;
    private final StudentRepository studentRepository;
    private final ConversionService conversionService;

    @Override
    public List<DrivingDto> findByDate(Date date) {
        Date drivingDate = addDays(date, 1);
        List<Driving> drivings = drivingRepository.findByStartDate(drivingDate);
        return drivings.stream().map(
                driving -> conversionService.convert(driving, DrivingDto.class))
                .collect(Collectors.toList());
    }

    @Override
    @Transactional
    public void createDriving(DrivingCreateDto dto) {
        Driving driving = new Driving();
        Admin admin = adminRepository.findById(dto.getAdminId()).orElseThrow(() -> new EntityNotFoundException("Заданий інструктор не існує"));
        if (!drivingRepository.existsByAdminAndStartTime(admin, dto.getStartTime())) {
            driving.setAdmin(admin);
            driving.setStudent(studentRepository.findById(dto.getStudentId()).orElseThrow(() -> new EntityNotFoundException("Заданий студент не існує")));
            driving.setStartTime(dto.getStartTime());
            drivingRepository.save(driving);
        } else {
            throw new InvalidParamException("Інструктор вже має екзамен у заданий час");
        }
    }

    private Date addDays(Date date, int days) {
        return new Date(date.getTime() + (1000 * 60 * 60 * 24 * days));
    }
}

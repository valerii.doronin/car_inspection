package com.inspection.driver.service.impl;

import com.inspection.driver.dto.DrivingFormDto;
import com.inspection.driver.dto.UserFullNameDto;
import com.inspection.driver.model.Admin;
import com.inspection.driver.model.Student;
import com.inspection.driver.repository.UserRepository;
import com.inspection.driver.service.UserService;
import lombok.AllArgsConstructor;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final ConversionService conversionService;

    @Override
    @Transactional
    public DrivingFormDto getDrivingForm() {
        List<Student> students = userRepository.findAllStudentPassedTest();
        List<Admin> admins = userRepository.findAllAdmins();
        return buildDto(students, admins);
    }

    private DrivingFormDto buildDto(List<Student> students, List<Admin> admins) {
        DrivingFormDto dto = new DrivingFormDto();
        dto.setStudents(students.stream().map(stud -> conversionService.convert(stud, UserFullNameDto.class)).collect(Collectors.toList()));
        dto.setAdmins(admins.stream().map(stud -> conversionService.convert(stud, UserFullNameDto.class)).collect(Collectors.toList()));
        return dto;
    }
}

package com.inspection.driver.service;

import com.inspection.driver.dto.DrivingFormDto;

public interface UserService {

    DrivingFormDto getDrivingForm();

}

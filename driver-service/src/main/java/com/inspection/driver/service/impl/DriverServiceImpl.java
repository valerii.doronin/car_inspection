package com.inspection.driver.service.impl;

import com.inspection.driver.dto.CarAddDto;
import com.inspection.driver.dto.DriverGeneralDto;
import com.inspection.driver.dto.DriverViolationDto;
import com.inspection.driver.exceptions.EntityNotFoundException;
import com.inspection.driver.model.Car;
import com.inspection.driver.model.Driver;
import com.inspection.driver.repository.CarRepository;
import com.inspection.driver.repository.DriverRepository;
import com.inspection.driver.service.DriverService;
import com.mifmif.common.regex.Generex;
import lombok.AllArgsConstructor;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class DriverServiceImpl implements DriverService {

    private final ConversionService conversionService;
    private final DriverRepository driverRepository;
    private final CarRepository carRepository;

    @Override
    public List<DriverGeneralDto> findGeneralInfo() {
        return driverRepository.findGeneralInfo()
                .stream().map(driver -> conversionService.convert(driver, DriverGeneralDto.class))
                .collect(Collectors.toList());
    }

    @Override
    public DriverViolationDto findById(Long id) {
        Driver driver = driverRepository.findById(id).orElseThrow(() -> new EntityNotFoundException("Водія не знайдено"));
        return conversionService.convert(driver, DriverViolationDto.class);
    }

    @Override
    @Transactional
    public String addCar(CarAddDto carDto) {
        Driver driver = driverRepository.findByLicenceNumber(carDto.getLicence())
                .orElseThrow(() -> new EntityNotFoundException("Водія із заданим номер посвідчення не існує"));
        String carNumber = generateTransportNumber(carDto.getRegion());
        Car car = new Car();
        car.setDriver(driver);
        car.setModel(carDto.getTransport());
        car.setNumber(carNumber);
        carRepository.save(car);
        return carNumber;
    }

    private String generateTransportNumber(String region) {
        Generex generex = new Generex("\\d{4}[А-ЕЖ-ЩЮЯІ]{2}");
        String number = region + generex.random();
        Car car = carRepository.findByNumber(number);
        while (car != null) {
            number = generex.random();
            car = carRepository.findByNumber(number);
        }
        return number;
    }
}

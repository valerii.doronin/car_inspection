package com.inspection.driver.repository;

import com.inspection.driver.model.Admin;
import com.inspection.driver.model.Student;
import com.inspection.driver.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface UserRepository extends JpaRepository<User, Long> {

    @Query("SELECT DISTINCT s from Student s "
            + "LEFT JOIN FETCH s.roles "
            + "WHERE s.passedTest = true "
            + "ORDER BY s.surname, s.name")
    List<Student> findAllStudentPassedTest();

    @Query("SELECT DISTINCT a from Admin a "
            + "LEFT JOIN FETCH a.roles "
            + "ORDER BY a.surname, a.name")
    List<Admin> findAllAdmins();

}

package com.inspection.driver.repository;

import com.inspection.driver.model.Car;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CarRepository extends JpaRepository<Car, Long> {

    Car findByNumber(String number);

}

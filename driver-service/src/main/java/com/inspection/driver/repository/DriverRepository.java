package com.inspection.driver.repository;

import com.inspection.driver.model.Driver;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

public interface DriverRepository extends JpaRepository<Driver, Long> {

    @Query("SELECT DISTINCT d from Driver d "
            + "JOIN FETCH d.licence l "
            + "LEFT JOIN FETCH d.cars cr "
            + "LEFT JOIN FETCH d.categories "
            + "LEFT JOIN d.violations "
            + "ORDER BY d.surname, d.name")
    List<Driver> findGeneralInfo();

    @Query("SELECT d from Driver d "
            + "LEFT JOIN FETCH d.violations v "
            + "LEFT JOIN FETCH v.article "
            + "WHERE d.id = :id")
    Optional<Driver> findById(@Param("id") Long id);

    @Query("SELECT d from Driver d "
            + "WHERE d.licence = "
            + "(SELECT l from Licence l "
            + "WHERE l.number = :licence)")
    Optional<Driver> findByLicenceNumber(String licence);

    @Query("SELECT DISTINCT d from Driver d "
            + "LEFT JOIN FETCH d.violations v "
            + "LEFT JOIN FETCH d.licence "
            + "LEFT JOIN FETCH v.article "
            + "WHERE d in "
            + "(SELECT v.driver from Violation v "
            + "GROUP BY v.driver "
            + "HAVING COUNT(*) > 2) "
            + "AND (d.deprived = null "
            + "OR d.deprived < :now)")
    List<Driver> findAllForDepriving(@Param("now") LocalDateTime now);

    @Query("SELECT DISTINCT d from Driver d "
            + "LEFT JOIN FETCH d.violations v "
            + "LEFT JOIN FETCH d.licence "
            + "LEFT JOIN FETCH v.article "
            + "WHERE d in "
            + "(SELECT v.driver from Violation v "
            + "GROUP BY v.driver "
            + "HAVING COUNT(*) < 3) "
            + "AND d.deprived != null "
            + "AND d.deprived < :now")
    List<Driver> findAllForRestoring(@Param("now") LocalDateTime now);
}
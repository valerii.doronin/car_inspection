package com.inspection.driver.repository;

import com.inspection.driver.model.Admin;
import com.inspection.driver.model.Driving;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

public interface DrivingRepository extends JpaRepository<Driving, Long> {

    @Query("SELECT DISTINCT d from Driving d "
            + "LEFT JOIN FETCH d.admin a "
            + "LEFT JOIN FETCH a.roles "
            + "LEFT JOIN FETCH d.student s "
            + "LEFT JOIN FETCH s.roles "
            + "WHERE CAST(d.startTime as date) = :startDate "
            + "ORDER BY d.startTime")
    List<Driving> findByStartDate(@Param("startDate") Date startDate);

    boolean existsByAdminAndStartTime(Admin admin, LocalDateTime startTime);
}

package com.inspection.driver.dto;

import lombok.Data;

@Data
public class ViolationDto {

    private Long id;
    private String articleNumber;
    private String articleDescription;
    private int articleCost;
    private String date;

}

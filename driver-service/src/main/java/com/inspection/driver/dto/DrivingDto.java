package com.inspection.driver.dto;

import lombok.Data;

@Data
public class DrivingDto {

    private Long id;
    private String adminSurname;
    private String adminName;
    private String studentSurname;
    private String studentName;
    private String studentPassport;
    private String startTime;

}

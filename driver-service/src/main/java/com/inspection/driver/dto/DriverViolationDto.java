package com.inspection.driver.dto;

import lombok.Data;

import java.util.List;

@Data
public class DriverViolationDto {

    private long id;
    private String surname;
    private String name;
    private List<ViolationDto> violations;

}

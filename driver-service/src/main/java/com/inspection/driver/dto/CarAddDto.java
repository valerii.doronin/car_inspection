package com.inspection.driver.dto;

import lombok.Data;

@Data
public class CarAddDto {

    private String transport;
    private String licence;
    private String region;

}

package com.inspection.driver.dto;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class DrivingCreateDto {

    private Long adminId;
    private Long studentId;
    private LocalDateTime startTime;

}

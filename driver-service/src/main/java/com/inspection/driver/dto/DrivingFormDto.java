package com.inspection.driver.dto;

import lombok.Data;

import java.util.List;

@Data
public class DrivingFormDto {

    List<UserFullNameDto> students;
    List<UserFullNameDto> admins;

}

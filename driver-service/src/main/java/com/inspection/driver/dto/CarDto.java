package com.inspection.driver.dto;

import lombok.Data;

@Data
public class CarDto {

    private long id;
    private String model;
    private String number;

}

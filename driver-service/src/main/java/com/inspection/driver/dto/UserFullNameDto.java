package com.inspection.driver.dto;

import lombok.Data;

@Data
public class UserFullNameDto {

    private Long id;
    private String surname;
    private String name;

}

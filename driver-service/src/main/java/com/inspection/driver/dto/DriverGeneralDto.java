package com.inspection.driver.dto;

import lombok.Data;

import java.util.List;

@Data
public class DriverGeneralDto {

    private long id;
    private String surname;
    private String name;
    private String passport;
    private boolean deprived;
    private String birthday;
    private String licenceNumber;
    private String licenceTerm;
    private List<CarDto> transport;
    private String categories;

}

package com.inspection.driver.converters;

import com.inspection.driver.dto.CarDto;
import com.inspection.driver.model.Car;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;

@Component
public class CarToDtoConverter implements Converter<Car, CarDto> {

    @Override
    public CarDto convert(@NotNull Car car) {
        CarDto dto = new CarDto();
        dto.setId(car.getId());
        dto.setModel(car.getModel());
        dto.setNumber(car.getNumber());
        return dto;
    }
}

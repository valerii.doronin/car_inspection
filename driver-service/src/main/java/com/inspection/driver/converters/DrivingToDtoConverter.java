package com.inspection.driver.converters;

import com.inspection.driver.dto.DrivingDto;
import com.inspection.driver.model.Driving;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;
import java.time.format.DateTimeFormatter;

@Component
public class DrivingToDtoConverter implements Converter<Driving, DrivingDto> {

    @Override
    public DrivingDto convert(@NotNull Driving driving) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("kk:mm");
        DrivingDto dto = new DrivingDto();
        dto.setId(driving.getId());
        dto.setStartTime(driving.getStartTime().format(formatter));
        dto.setAdminSurname(driving.getAdmin().getSurname());
        dto.setAdminName(driving.getAdmin().getName());
        dto.setStudentSurname(driving.getStudent().getSurname());
        dto.setStudentName(driving.getStudent().getName());
        dto.setStudentPassport(driving.getStudent().getPassport());
        return dto;
    }
}

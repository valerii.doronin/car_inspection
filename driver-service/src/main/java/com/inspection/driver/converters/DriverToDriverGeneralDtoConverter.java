package com.inspection.driver.converters;

import com.inspection.driver.dto.DriverGeneralDto;
import com.inspection.driver.model.Driver;
import com.inspection.driver.model.LicenceCategory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;
import java.time.format.DateTimeFormatter;
import java.util.stream.Collectors;

@Component
public class DriverToDriverGeneralDtoConverter implements Converter<Driver, DriverGeneralDto> {

    @Autowired
    private CarToDtoConverter converter;

    @Override
    public DriverGeneralDto convert(@NotNull Driver driver) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");

        DriverGeneralDto dto = new DriverGeneralDto();
        dto.setId(driver.getId());
        dto.setSurname(driver.getSurname());
        dto.setName(driver.getName());
        dto.setDeprived(driver.getDeprived() != null);
        dto.setPassport(driver.getPassport());

        dto.setBirthday(driver.getBirthday().format(formatter));
        dto.setLicenceNumber(driver.getLicence().getNumber());
        dto.setLicenceTerm(driver.getLicence().getStartDate().format(formatter)
                + " - "
                + driver.getLicence().getFinishDate().format(formatter));
        dto.setCategories(driver.getCategories().stream().map(LicenceCategory::getName).collect(Collectors.joining(", ")));
        dto.setTransport(driver.getCars().stream().map(converter::convert).collect(Collectors.toList()));
        return dto;
    }
}

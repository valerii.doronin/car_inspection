package com.inspection.driver.converters;

import com.inspection.driver.dto.UserFullNameDto;
import com.inspection.driver.model.User;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class UserToFullNameDtoConverter implements Converter<User, UserFullNameDto> {

    @Override
    public UserFullNameDto convert(User user) {
        UserFullNameDto dto = new UserFullNameDto();
        dto.setId(user.getId());
        dto.setSurname(user.getSurname());
        dto.setName(user.getName());
        return dto;
    }
}

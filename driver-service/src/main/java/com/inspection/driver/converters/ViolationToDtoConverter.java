package com.inspection.driver.converters;

import com.inspection.driver.dto.ViolationDto;
import com.inspection.driver.model.Article;
import com.inspection.driver.model.Violation;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;
import java.time.format.DateTimeFormatter;

@Component
public class ViolationToDtoConverter implements Converter<Violation, ViolationDto> {

    @Override
    public ViolationDto convert(@NotNull Violation violation) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");

        ViolationDto dto = new ViolationDto();
        Article article = violation.getArticle();
        dto.setId(violation.getId());
        dto.setDate(violation.getTime().format(formatter));
        dto.setArticleCost(article.getCost());
        dto.setArticleDescription(article.getDescription());
        dto.setArticleNumber(article.getArticle());
        return dto;
    }
}

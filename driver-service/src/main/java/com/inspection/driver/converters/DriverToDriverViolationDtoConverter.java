package com.inspection.driver.converters;

import com.inspection.driver.dto.DriverViolationDto;
import com.inspection.driver.model.Driver;
import com.inspection.driver.model.Violation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;
import java.util.Comparator;
import java.util.stream.Collectors;

@Component
public class DriverToDriverViolationDtoConverter implements Converter<Driver, DriverViolationDto> {

    @Autowired
    private ViolationToDtoConverter converter;

    @Override
    public DriverViolationDto convert(@NotNull Driver driver) {
        DriverViolationDto dto = new DriverViolationDto();
        dto.setId(driver.getId());
        dto.setSurname(driver.getSurname());
        dto.setName(driver.getName());
        dto.setViolations(
                driver.getViolations().stream()
                        .sorted(Comparator.comparing(Violation::getTime))
                        .map(e -> converter.convert(e)).collect(Collectors.toList()));
        return dto;
    }
}

package com.inspection.gateway.security;

import lombok.Getter;
import lombok.ToString;
import org.springframework.beans.factory.annotation.Value;

@Getter
@ToString
public class JwtAuthenticationConfig {

    @Value("${inspection.security.jwt.url:/login/**}")
    private String authUrl;

    @Value("${inspection.security.jwt.adminUrl:/secure/admin/**}")
    private String adminUrl;

    @Value("${inspection.security.jwt.mobileAdminUrl:/secure/student/drivings/**}")
    private String mobileAdminUrl;

    @Value("${inspection.security.jwt.studentUrl:/secure/student/**}")
    private String studentUrl;

    @Value("${inspection.security.jwt.header:Authorization}")
    private String header;

    @Value("${inspection.security.jwt.prefix:Bearer}")
    private String prefix;

    @Value("${jwt.token.expired}")
    private int expiration; // default 24 hours

    @Value("${jwt.token.secret}")
    private String secret;
}

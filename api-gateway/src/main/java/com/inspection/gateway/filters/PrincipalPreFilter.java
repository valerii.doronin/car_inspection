package com.inspection.gateway.filters;

import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import com.netflix.zuul.http.HttpServletRequestWrapper;
import com.netflix.zuul.http.ServletInputStreamWrapper;
import org.apache.commons.lang.StringUtils;
import org.springframework.security.web.servletapi.SecurityContextHolderAwareRequestWrapper;
import org.springframework.stereotype.Component;
import org.springframework.util.ReflectionUtils;
import org.springframework.util.StreamUtils;

import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServletRequest;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Field;
import java.nio.charset.StandardCharsets;

@Component
public class PrincipalPreFilter extends ZuulFilter {

    @Override
    public String filterType() {
        return "pre";
    }

    @Override
    public int filterOrder() {
        return 0;
    }

    @Override
    public boolean shouldFilter() {
        return RequestContext.getCurrentContext().getRequest().getRequestURI().startsWith("/secure/student");
    }

    @Override
    public Object run() {

        RequestContext context = RequestContext.getCurrentContext();
        HttpServletRequest request = new HttpServletRequestWrapper(context.getRequest());

        String requestData = null;
        RequestContext ctx = RequestContext.getCurrentContext();
        InputStream in = (InputStream) ctx.get("requestEntity");
        try {
            if (in == null) {
                in = ctx.getRequest().getInputStream();
            }
            requestData = StreamUtils.copyToString(in, StandardCharsets.UTF_8);
        } catch (IOException e) {
            e.printStackTrace();
        }
        String modifiedRequest = wrapRequestEntity(requestData);
        HttpServletRequestWrapper requestWrapper = modifyRequest(request, modifiedRequest);
        context.setRequest(requestWrapper);
        return null;
    }

    private HttpServletRequestWrapper modifyRequest(HttpServletRequest request, String body) {
        return new HttpServletRequestWrapper(request) {
            @Override
            public byte[] getContentData() {
                return body.getBytes(StandardCharsets.UTF_8);
            }

            @Override
            public int getContentLength() {
                return body.getBytes().length;
            }

            @Override
            public long getContentLengthLong() {
                return body.getBytes().length;
            }

            @Override
            public BufferedReader getReader() throws IOException {
                return new BufferedReader(new InputStreamReader(new ByteArrayInputStream(body.getBytes(StandardCharsets.UTF_8))));
            }

            @Override
            public ServletInputStream getInputStream() throws UnsupportedEncodingException {
                return new ServletInputStreamWrapper(body.getBytes(StandardCharsets.UTF_8));
            }
        };
    }

    private String wrapRequestEntity(String body) {
        String wrappedBody = "{\"principal\":\"" +
                getPrincipal() +
                "\"";
        if (StringUtils.isBlank(body)) {
            return wrappedBody
                    + "}";
        } else {
            return wrappedBody
                    + ","
                    + body.substring(1, body.length() - 1).replace("\n", "").replace("\t", "").replace("\r", "")
                    + "}";
        }
    }

    private String getPrincipal() {
        HttpServletRequest request = RequestContext.getCurrentContext().getRequest();
        Field requestField = ReflectionUtils.findField(request.getClass(), "request");
        requestField.setAccessible(true);
        SecurityContextHolderAwareRequestWrapper wrapper = (SecurityContextHolderAwareRequestWrapper) ReflectionUtils.getField(requestField, request);
        return wrapper.getUserPrincipal().getName();
    }
}

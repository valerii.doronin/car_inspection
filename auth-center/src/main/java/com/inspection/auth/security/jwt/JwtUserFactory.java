package com.inspection.auth.security.jwt;

import com.inspection.auth.model.Role;
import com.inspection.auth.model.User;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.util.List;
import java.util.stream.Collectors;

public final class JwtUserFactory {

    public static JwtUser create(User user) {
        return new JwtUser(user.getId(),
                user.getSurname(),
                user.getName(),
                user.getEmail(),
                user.getPassword(),
                createGrantedAuthority(user.getRoles()));
    }

    private static List<GrantedAuthority> createGrantedAuthority(List<Role> userRoles) {
        return userRoles.stream().map(Role::getName).map(SimpleGrantedAuthority::new).collect(Collectors.toList());
    }
}

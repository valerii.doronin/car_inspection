package com.inspection.auth.service;

import com.inspection.auth.model.User;

public interface UserService {

    User findByEmail(String email);

}

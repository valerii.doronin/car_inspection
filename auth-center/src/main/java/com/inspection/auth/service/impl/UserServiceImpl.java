package com.inspection.auth.service.impl;

import com.inspection.auth.model.User;
import com.inspection.auth.repository.UserRepository;
import com.inspection.auth.service.UserService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;

    @Override
    public User findByEmail(String email) {
        return userRepository.findByEmail(email);
    }
}
